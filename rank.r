# library(tidyverse)
library(ggplot2)
library(svglite)
dev.list()

ranks <- read.csv("rank.csv")
print(ranks)
print(is.data.frame(ranks))
print(ncol(ranks))
print(nrow(ranks))
str(ranks)
print(summary(ranks))

str(data.frame(
    as.Date(ranks$date),
    ranks$rank
))

svglite("rank.svg")
print(ggplot(
    data = data.frame(
        as.Date(ranks$date),
        ranks$rank
    ),
    aes(as.Date.ranks.date., ranks.rank)
) + geom_point() + labs(x = "Date", y = "Rank"))
dev.off()

svglite("referenced_by.svg")
print(ggplot(
    data = data.frame(
        date = as.Date(ranks$date),
        refb = ranks$referenced_by
    ),
    aes(date, refb)
) + geom_point() + labs(x = "Date", y = "Referenced by"))
dev.off()
