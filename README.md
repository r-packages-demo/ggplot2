# ggplot2

A system for 'declaratively' creating graphics, based on "The Grammar of Graphics". [ggplot2](https://cran.r-project.org/package=ggplot2)

# Official documentation
* [*ggplot2*](https://ggplot2.tidyverse.org)
  * [*3 Data visualisation*
    ](https://r4ds.had.co.nz/data-visualisation.html)

# Unofficial documentation
* [*R graphics with ggplot2 workshop notes*
  ](http://tutorials.iq.harvard.edu/R/Rgraphics/Rgraphics.html)
* [*How to save a plot made with ggplot2 as SVG*
  ](https://stackoverflow.com/questions/12226822/how-to-save-a-plot-made-with-ggplot2-as-svg)
  (2013-2019)
* [*Plot Time Series Data Using GGPlot*
  ](http://www.sthda.com/english/articles/32-r-graphics-essentials/128-plot-time-series-data-using-ggplot/)
  2017-11 kassambara
* (fr) [*Introduction à ggplot2, la grammaire des graphiques*
  ](http://larmarange.github.io/analyse-R/intro-ggplot2.html)
  François Briatte

# Notes
* Aesthetics must be valid data columns.

# [Results](https://r-packages-demo.gitlab.io/ggplot2)
## Debian
<figure>
  <img
    src="https://r-packages-demo.gitlab.io/ggplot2/debian/rank.svg"
    alt="Debian rank"
    width="40%"
  >
  <img
    src="https://r-packages-demo.gitlab.io/ggplot2/debian/referenced_by.svg"
    alt="Debian referenced_by"
    width="40%"
  >
</figure> 
